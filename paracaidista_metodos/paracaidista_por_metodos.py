%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np

g=9.8
m=68.1
c=12.5
vel=0
cont=0
ti=[]
ti.append(0)
res=[]

for t in range(0, 200, 2):
    cont+=1
    ti.append(t)
    t2=ti[cont]
    t1=ti[cont-1]
    x=vel+((g-(c/m)*vel)*(t2-t1))
    res.append(x)
    print "cuando el tiempo es: "+str(t2)+ " segundos, la velocidad del paracaidasta es: "+str(x)
    vel=x
    

plt.plot(res)
plt.title('Velocidad Del Paracaidista')
plt.ylabel('Velocidad')
plt.xlabel('Tiempo (Seg)')
    


