%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
import math
gr=9.8
masa=68.1
coefi=12.5
res=[]
for t in range (200):
    v=(gr*masa/coefi)*(1-(math.pow(math.e,-coefi*t/masa)))
    print "cuando el tiempo es: "+str(t)+ ", la velocidad del paracaidasta es:"+str(v)
    res.append(v)
plt.plot(res)
plt.title('Velocidad Del Paracaidista')
plt.ylabel('Velocidad')
plt.xlabel('Tiempo (Seg)')